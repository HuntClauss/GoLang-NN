package main

import (
	"GoLang-NN/network"
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	n := network.NewNetwork()

	n.AddLayer(2, network.ActivationTanh)
	n.AddLayer(30, network.ActivationRelu)
	n.AddLayer(1, network.ActivationTanh)
	n.Setup()

	out := n.Predict([]float64{0.312, -0.32})
	fmt.Println(out)
}
