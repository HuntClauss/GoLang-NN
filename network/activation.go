package network

import "math"

type ActivationType int

const (
	ActivationLinear  ActivationType = 0
	ActivationNone    ActivationType = 0
	ActivationSigmoid ActivationType = 1
	ActivationTanh    ActivationType = 2
	ActivationRelu    ActivationType = 3
)

func GetActivation(a ActivationType) ActivationFunction {
	switch a {
	case ActivationLinear | ActivationNone:
		return Linear{}
	case ActivationSigmoid:
		return Sigmoid{}
	case ActivationTanh:
		return Tanh{}
	case ActivationRelu:
		return Relu{}
	}
	return Linear{}
}

type ActivationFunction interface {
	N(float64) float64
	D(float64) float64
}

type Sigmoid struct{}

func (a Sigmoid) N(x float64) float64 { return Logistic(x, 1) }
func (a Sigmoid) D(y float64) float64 { return y * (1 - y) }
func Logistic(x, a float64) float64   { return 1 / (1 + math.Exp(-a*x)) }

type Tanh struct{}

func (a Tanh) N(x float64) float64 { return (1 - math.Exp(-2*x)) / (1 + math.Exp(-2*x)) }
func (a Tanh) D(y float64) float64 { return 1 - math.Pow(y, 2) }

type Relu struct{}

func (a Relu) N(x float64) float64 { return math.Max(x, 0) }
func (a Relu) D(y float64) float64 {
	if y > 0 {
		return 1
	}
	return 0
}

type Linear struct{}

func (a Linear) N(x float64) float64 { return x }
func (a Linear) D(y float64) float64 { return 1 }
