package network

type Neuron struct {
	InSynapses  []*Synapse
	OutSynapses []*Synapse
	Activation  ActivationType
	Out         float64
}

func NewNeuron(act ActivationType) *Neuron {
	return &Neuron{Activation: act}
}

func (n *Neuron) ConnectTo(nTo *Neuron, weight float64) {
	NewSynapseConnection(n, nTo, weight)
}

func (n *Neuron) activate(x float64) float64 {
	return GetActivation(n.Activation).N(x)
}

func (n *Neuron) Calculate() {
	var sum float64

	for _, s := range n.InSynapses {
		sum += s.Out
	}
	n.Out = n.activate(sum)

	for _, s := range n.OutSynapses {
		s.Signal(n.Out)
	}
}
