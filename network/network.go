package network

import (
	"math/rand"
)

type Network struct {
	Layers []*Layer
	Input  int
}

func NewNetwork() *Network {
	return &Network{}
}

func (net *Network) AddLayer(neurons int, act ActivationType) {
	net.Layers = append(net.Layers, NewLayer(neurons, act))
}

func (net *Network) Setup() {
	net.connectLayers()
	net.setUpInput()
	net.randomizeSynapses()
	net.calculate()
}

func (net *Network) connectLayers() {
	for i := 0; i < len(net.Layers)-1; i++ {
		net.Layers[i].Connect(net.Layers[i+1])
	}
}

func (net *Network) randomizeSynapses() {
	for _, l := range net.Layers {
		for _, n := range l.Neurons {
			for _, s := range n.InSynapses {
				s.Weight = rand.Float64()*2 - 1
			}
		}
	}
}

func (net *Network) setUpInput() {
	for _, n := range net.Layers[1].Neurons {
		n.InSynapses = make([]*Synapse, len(net.Layers[0].Neurons))
		for i := range n.InSynapses {
			n.InSynapses[i] = NewSynapse(0)
		}
	}
}

func (net *Network) calculate() {
	for _, l := range net.Layers {
		l.Calculate()
	}
}

func (net *Network) forward(input []float64) error {
	for _, n := range net.Layers[1].Neurons {
		for i := 0; i < len(input); i++ {
			n.InSynapses[i].Signal(input[i])
		}
	}
	net.calculate()
	return nil
}

func (net *Network) Predict(input []float64) []float64 {
	net.forward(input)

	outLayer := net.Layers[len(net.Layers)-1]
	output := make([]float64, len(outLayer.Neurons))
	for i, n := range outLayer.Neurons {
		output[i] = n.Out
	}
	return output
}
