package network

type Synapse struct {
	Weight float64
	In     float64
	Out    float64
}

func NewSynapse(weight float64) *Synapse {
	return &Synapse{Weight: weight}
}

func NewSynapseConnection(from, to *Neuron, weight float64) {
	syn := NewSynapse(weight)

	from.OutSynapses = append(from.OutSynapses, syn)
	to.InSynapses = append(to.InSynapses, syn)
}

func (s *Synapse) Signal(input float64) {
	s.In = input
	s.Out = s.In * s.Weight
	//fmt.Println(s.Out)
}
