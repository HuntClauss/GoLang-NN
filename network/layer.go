package network

type Layer struct {
	Neurons    []*Neuron
	Activation ActivationType
}

func NewLayer(n int, act ActivationType) *Layer {
	l := &Layer{Activation: act}
	for i := 0; i < n; i++ {
		l.addNeuron(act)
	}
	return l
}

func (l *Layer) addNeuron(act ActivationType) {
	n := NewNeuron(act)
	l.Neurons = append(l.Neurons, n)
}

func (l *Layer) Calculate() {
	for _, n := range l.Neurons {
		n.Calculate()
	}
}

func (l *Layer) Connect(to *Layer) {
	for _, n := range l.Neurons {
		for _, nTo := range to.Neurons {
			n.ConnectTo(nTo, 0)
		}
	}
}
